#include "fatorial.h"

int fatorial(int x){
	/* Escreva seu código aqui */
	int i,aux;
	
	aux = x;
	
	if(x > 0){
		for(i = 1; i < x;i++){
			aux = aux * i;
		}
		return aux;
	}
	return -1;
}
